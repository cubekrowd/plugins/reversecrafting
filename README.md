# ReverseCrafting

Right click with an item in hand to drop certain items.

## Configuration

In the `recipes` section, add sections (keys) corresponding to item names
as given in the [Material enum documentation][material]. Their contents
should be another mapping of material names to amounts (integer).

Example:

```yaml
recipes:
    magma_cream:
        slime_ball: 1
        blaze_powder: 1
```

will allow a user to right click while holding magma cream to
consume the magma cream and drop a slime ball and blaze powder.

## License

```
Copyright (C) 2018 Kapurai (Lekro)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```


[material]: https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html
