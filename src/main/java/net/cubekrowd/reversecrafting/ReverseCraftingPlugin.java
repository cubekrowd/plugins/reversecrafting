/*
 * Copyright (C) 2018 Kapurai (Lekro)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package net.cubekrowd.reversecrafting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class ReverseCraftingPlugin extends JavaPlugin implements Listener {
    
    private Map<Material, List<ItemStack>> recipes;
    
    @Override
    public void onEnable() {
        
        saveDefaultConfig();
        ConfigurationSection recipesConfig = getConfig().getConfigurationSection("recipes");
        
        recipes = new HashMap<>();
        
        for (String input : recipesConfig.getKeys(false)) {
            ConfigurationSection innerRecipe = recipesConfig.getConfigurationSection(input);
            
            List<ItemStack> outputs = new ArrayList<>(innerRecipe.getKeys(false).size());
            for (String output : innerRecipe.getKeys(false)) {
                Material m = Material.matchMaterial(output);
                if (m == null) {
                    getLogger().warning("In recipe for " + input + ", found invalid item " + output);
                    continue;
                }
                ItemStack i = new ItemStack(m);
                i.setAmount(innerRecipe.getInt(output));
                outputs.add(i);
            }
            
            Material m = Material.matchMaterial(input);
            if (m == null) {
                getLogger().warning("In recipe for " + input + ", found invalid item " + input);
                continue;
            }
            
            getLogger().info("Registered reverse recipe for " + input);
            recipes.put(m, outputs);
        }
        
        getServer().getPluginManager().registerEvents(this, this);
        
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent ev) {
        
        if (ev.getAction() != Action.RIGHT_CLICK_AIR && ev.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }
        
        Player p = ev.getPlayer();
        ItemStack item = ev.getItem();
        if (item == null) return;
        Material mat = item.getType();
        
        if (recipes.containsKey(item.getType())) {
            item.setAmount(item.getAmount()-1);
            for (ItemStack i : recipes.get(mat)) {
                p.getWorld().dropItem(p.getLocation(), i);
            }
        }
        
    }
    
    @Override
    public void onDisable() {
        
    }

}
